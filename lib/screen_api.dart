import 'dart:math';

import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ScreenApi extends StatefulWidget {
  const ScreenApi({Key? key}) : super(key: key);

  @override
  State<ScreenApi> createState() => _ScreenApiState();
}

class _ScreenApiState extends State<ScreenApi> {
  fecthPost() {
    return http.get(Uri.parse("https://jsonplaceholder.typicode.com/posts/1"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Api"),
      ),
      body: (Center(
        child: FutureBuilder(
          future: fecthPost(),
          builder: (context, snapshot) {
            final connState = snapshot.connectionState;
            switch (connState){

              case ConnectionState.none:
                // TODO: Handle this case.
                break;
              case ConnectionState.waiting:
                // TODO: Handle this case.
                break;
              case ConnectionState.active:
                // TODO: Handle this case.
                break;
              case ConnectionState.done:
                // TODO: Handle this case.
                if(snapshot.hasData){
                  final response = snapshot.data as http.Response;
                  final data = json.decode(response.body);
                  return Text(data['title']);
                }
                break;
            }
            
             
            return const CircularProgressIndicator();
          },
        ),
      )),
    );
  }
}
